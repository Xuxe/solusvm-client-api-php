<?PHP
/*
* Copyright (C) 2015 by Xuxe (Julian H�benthal) 
* xuxe@xuxe-network.eu
*/

/*
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>
 * 
 */
require("../SolusVM-Client.php");

$instances = array(
    
   array("url" => "NODE_1_ADDR", "key" => "", "hash" => ""),
   array("url" => "NODE_2_ADDR", "key" => "", "hash" => ""),
        
 );

echo "<pre>";
var_dump(new SOLUSVM_MULTINODE_STATUS_QUERY($instances));
echo "</pre>";
?>