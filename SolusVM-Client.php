<?PHP
/*
* SOLUSVM CLIENT for the SOLUSVM CLIENT API.
* Copyright (C) 2015 by Xuxe (Julian Hübenthal) 
* xuxe@xuxe-network.eu
*/

/*
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>
 * 
 */

class SOLUSVM_CLIENT
{
    private $data = array();
    private $node;
    private $result = array();
    private $handler;
    
    
    public $error = false;
    private $error_msg;
	
    /*  Basic constructor.
     * 
     * @return Object
     * 
     */
    
    public function __construct($node_addr, $api_key, $api_hash)
    {
                if(!is_string($node_addr) || !is_string($api_key) || !is_string($api_hash))
                {
                    throw new Exception('Class needs to be initalized with strings.');
                }

                $this->data["key"] = $api_key;
                $this->data["hash"] = $api_hash;
                $this->node = $node_addr;
                
           SOLUSVM_CLIENT::SetupCurl();
    }
    

    
    /*
     * Curl controller setup.
     * Set basic curl options and select the Solus Node.
     * 
     * @return bool
     */
	 
    private function SetupCurl()
    {
        
                try {
                    
                        $this->handler = curl_init();

                        curl_setopt($this->handler, CURLOPT_URL, $this->node."/command.php");
                        curl_setopt($this->handler, CURLOPT_POST, 1);
                        curl_setopt($this->handler, CURLOPT_TIMEOUT, 20);
                        curl_setopt($this->handler, CURLOPT_FRESH_CONNECT, 1);
                        curl_setopt($this->handler, CURLOPT_SSL_VERIFYPEER, false);
                        curl_setopt($this->handler, CURLOPT_SSL_VERIFYHOST, false);
                        curl_setopt($this->handler, CURLOPT_HEADER, 0);
                        curl_setopt($this->handler, CURLOPT_HTTPHEADER, array( 'Expect:') );
                        curl_setopt($this->handler, CURLOPT_RETURNTRANSFER, 1);

                        return true;
                
                }
                catch(Exception $e)
                {
                        throw new Exception($e->getMessage());
                   
                }
    }
    
    /*
     * Returns the result of the last request.
     * 
     * @return array
     */
    
    public function GetResult()
    {
        return $this->result;
    }
    
    /*
     * Extracts the result and build a array.
     * @return array
     */
	 
    private function ExtractResult($data)
    {
        $match = array();
        preg_match_all('/<(.*?)>([^<]+)<\/\\1>/i', $data, $match);
        foreach ($match[1] as $x => $y)
        {
            $this->result[$y] = $match[2][$x];
        }
    }
    
    /*
     * Select action methods: status, boot, reboot, info and shutdown.
     * 
     * @return bool
     */
    
    public function SetAction($action)
    {
        switch($action)
        {
            case "status":
                $this->data["action"] = "status";
                return true;
            
            case "reboot":
                $this->data["action"] = "reboot";
                return true;
      
            case "shutdown":
                $this->data["action"] = "shutdown";
                return true;
        
            case "boot":
                $this->data["action"] = "boot";
                return true;
            case "info":
                $this->data["action"] = "info";
                curl_setopt($this->handler, CURLOPT_URL, $this->node."/command.php?ipaddr=true&hdd=true&bw=true&mem=true");
                return true;
  
            default: 
                    return false;
         
        }
    }
	
	/* 
	 * Returns the error message.
	 * @return string
	 */
	 
	public function GetErrorMsg()
	{
		if($this->error)
		{
			return $this->error_msg;
		}
	}
    
    /*
     * Sets the postfield and sends the request to the solus node.
     * 
     * @return bool 
     */
	 
    public function Execute()
    {
        curl_setopt($this->handler, CURLOPT_POSTFIELDS, $this->data);
        
        if($data = curl_exec($this->handler))
        {
            SOLUSVM_CLIENT::ExtractResult($data);
            return true;
        }
        else {
			$this->error = true;
                        $this->error_msg = curl_error($this->handler);
			return false;
        }
        
        
        
    }

    
    public function __destruct()
    {
        curl_close($this->handler);
    }
    
}

/*
 * 
 * Class to query multiple nodes for status.
 * 
 * @return Object
 */

class SOLUSVM_MULTINODE_STATUS_QUERY {
    
    private $result = array();
    
    public function __construct($instances)
    {
            
            if(!is_array($instances))
            {
                throw new Exception('Class: SOLUSVM_MULTINODES_STATUS_QUERY needs a array as constructor.');
            }
            
            foreach ($instances as $instance) 
            {

                $node = new SOLUSVM_CLIENT($instance["url"], $instance["key"], $instance["hash"]);
                if($node->SetAction("status"))
                {
                        if($node->Execute())
                        {
                                array_push($this->result, $node->GetResult());

                        } elseif($node->error)
                        {
                                array_push($this->result, array("status" => "error", "statusmsg" => $node->GetErrorMsg()));
                        }
                }

            }
            
           
    }
}

/*
 * 
 * Class to query multiple nodes for info.
 * 
 * @return Object
 */

class SOLUSVM_MULTINODE_INFO_QUERY {
    
    private $result = array();
    
    public function __construct($instances)
    {
            
            if(!is_array($instances))
            {
                throw new Exception('Class: SOLUSVM_MULTINODES_INFO_QUERY needs a array as constructor.');
            }
            
            foreach ($instances as $instance) 
            {

                $node = new SOLUSVM_CLIENT($instance["url"], $instance["key"], $instance["hash"]);
                if($node->SetAction("info"))
                {
                        if($node->Execute())
                        {
                                array_push($this->result, $node->GetResult());

                        } elseif($node->error)
                        {
                                array_push($this->result, array("status" => "error", "statusmsg" => $node->GetErrorMsg()));
                        }
                }

            }
            
           
    }
}
?>